# CGP.py
# Approximation of arithmetic circuits in alternative representations, BIN, 29.4.2022
# Author: Matěj Kudera, xkuder04, VUT FIT
# CGP used for space optimalization of given arithmetic circuit

######################## Imports #########################
# Python
import argparse
from cmath import inf
import time
import json
import random

# Functions
import CGP_functions
import arithmetic_circuits

###################### Global variables ##################
# Selection of fitness function
optimaze_size = False
swith_fitness = False

######################## Functions #######################
# Read CGP inicialization from .json file
def get_inicialization(filename):
    f = open(filename)
    data = json.load(f)
    f.close()

    return data

# Read input chromosome if given or generate new randomly
def get_start_genotype(genotype_file, cgp_setup):
    if (genotype_file != None):
        # Read chromosome from given .json file
        f = open(genotype_file)
        genotype = json.load(f)
        f.close()

        return genotype
    else:
        # Generate random chromosome
        return get_random_genotype(cgp_setup)
        

# Generate chromosome randomly
def get_random_genotype(cgp_setup):
    # Iterate through all blocks of cgp
    genotype = []

    for x in range(cgp_setup["inputs"], (cgp_setup["columns"] * cgp_setup["rows"]) + cgp_setup["inputs"]):
        block = []

        # Inputs of actual block can be CGP inputs or blocks in L_back columns
        inputs_array = []

        for j in range(cgp_setup["inputs"]):
            inputs_array.append(j)
            
        last_possible_block = (cgp_setup["rows"] * (((x - cgp_setup["inputs"]) // cgp_setup["rows"]))) + cgp_setup["inputs"]
        first_possible_block = max(cgp_setup["inputs"], last_possible_block - (cgp_setup["l_back"] * cgp_setup["rows"]))
        #print("First_index =" + str(first_possible_block))
        #print("Last_index =" + str(last_possible_block))

        for j in range(first_possible_block, last_possible_block):
            inputs_array.append(j)

        # Generate block inputs
        for i in range(cgp_setup["block_inputs"]):
            # Pick random block from possible input blocks
            block.append(random.choice(inputs_array))

        # Pick block function from possible functions
        block.append(random.choice(cgp_setup["cgp_functions"]))

        # Append block to chromosome
        genotype.append(block)

        #print("Index: " + str(x) + " inputs: ")
        #print(inputs_array)
        #print("#############################")
            
    # Add outputs connection to chromosome
    outputs = []

    inputs_array = []
    last_possible_block = (cgp_setup["columns"] * cgp_setup["rows"]) + cgp_setup["inputs"]
    first_possible_block = max(cgp_setup["inputs"], last_possible_block - (cgp_setup["l_back"] * cgp_setup["rows"]))

    for j in range(cgp_setup["inputs"]):
        inputs_array.append(j)

    for j in range(first_possible_block, last_possible_block):
        inputs_array.append(j)

    # Generate outputs
    for i in range(cgp_setup["outputs"]):
        # Pick random block from possible input blocks
        outputs.append(random.choice(inputs_array))

    #print("Outputs: ")
    #print(inputs_array)
    #print("#############################")

    genotype.append(outputs)

    return genotype

# Generate start population of chromozomes
def get_start_population(population_size, genotype_file, cgp_setup):
    polulation = []

    for x in range(population_size):
        if (x == 0):
            polulation.append(get_start_genotype(genotype_file, cgp_setup))
        else:
            polulation.append(get_random_genotype(cgp_setup))

    return polulation

# Traverse used blocks in genotype
def traverse_block(genotype, block_num, cgp_setup):
    # Stop on cgp inputs
    if (block_num < cgp_setup["inputs"]):
        return []
    else:
        traversed_blocks = []
        traversed_blocks += [block_num]

        for x in range(0, cgp_setup["block_inputs"]):
            traversed_blocks += traverse_block(genotype, genotype[block_num - cgp_setup["inputs"]][x], cgp_setup)
        
        return traversed_blocks

# Get number of used blocks in genotype
def used_blocks(genotype, cgp_setup):
    used_blocks = []

    #print(genotype)
    # Travers genotype from output values
    for x in range(0, cgp_setup["outputs"]):
        used_blocks += traverse_block(genotype, genotype[-1][x], cgp_setup)

    # Make set from list
    used_blocks_unique =  set(used_blocks)

    return len(used_blocks_unique)

# Get output of circuit in genotype for given inputs
def circuit_output(genotype, input, cgp_setup):
    blocks_outputs = [None] * (cgp_setup["columns"] * cgp_setup["rows"] + cgp_setup["inputs"])

    # Inicialize CGP inputs
    for x in range(0, cgp_setup["inputs"]):
        blocks_outputs[x] = int(input[x])

    # Go over all blocks and calculate their output
    for x in range(cgp_setup["inputs"], (cgp_setup["columns"] * cgp_setup["rows"]) + cgp_setup["inputs"]):

        # Get input values for block in string
        block_inputs = ""

        for i in range(0, cgp_setup["block_inputs"]):
            block_inputs += str(blocks_outputs[genotype[x - cgp_setup["inputs"]][i]])

        # Get block output from specified function
        #print(block_inputs)
        blocks_outputs[x] = CGP_functions.cgp_truth_tables[genotype[x - cgp_setup["inputs"]][-1]][block_inputs]

    #print(blocks_outputs)

    # Return circuit output
    circuit_output = ""
    for x in range(0, cgp_setup["outputs"]):
        circuit_output += str(blocks_outputs[genotype[-1][x]])

    #print(circuit_output)

    return circuit_output

# Get fitness of genotype
def fitness(genotype, maximal_average_arithmetic_error, cgp_setup):
    cumulative_error = 0

    # Test every posible input of circuit
    for x in range(0, 2**cgp_setup["inputs"]):
        # Remove prefix 0b, and use given number of bits
        binary = str(bin(x))[2:].zfill(cgp_setup["inputs"])
        #print(binary)

        # Get genotype's output and expected output
        output = circuit_output(genotype, binary, cgp_setup)
        expected_output = arithmetic_circuits.circuits[cgp_setup["function"]][binary]

        # Calculate error for every output bit
        output_error = 0
        for i in range(0, cgp_setup["outputs"]):
            if (output[i] != expected_output[i]):
                output_error += 1

        cumulative_error += output_error/cgp_setup["outputs"]

    # Get average arithmetic error
    total_error = cumulative_error / 2**cgp_setup["inputs"]

    #print(total_error)
    #exit()

    # Use selected fitness
    global optimaze_size
    global swith_fitness
    if (optimaze_size):
        # If average arithmetic error is less then maximal allowed error return circuit's space as fitness
        if (total_error <= maximal_average_arithmetic_error):
            blocks = used_blocks(genotype, cgp_setup)
            return blocks
        else:
            return inf
    else:
        # Swith to size if average arithmetic error reached
        if (total_error <= maximal_average_arithmetic_error):
            swith_fitness = True

        return total_error


# Get fitness for every genotype in population
def get_population_fitness(population, maximal_average_arithmetic_error, cgp_setup):
    population_fitness = []

    for genotype in population:
        fitness_value = fitness(genotype, maximal_average_arithmetic_error, cgp_setup)
        population_fitness.append(fitness_value)

    return population_fitness

# Get best genotypes in population
def get_best_genotypes(population, population_fitness):
    best_genotypes = []
    best_value = inf

    # Get list of best genotypes
    for x in range(len(population)):
        if (population_fitness[x] < best_value):
            best_genotypes = []
            best_value = population_fitness[x]
            best_genotypes.append(population[x])
        elif (population_fitness[x] == best_value):
            best_genotypes.append(population[x])

    # Return best fitness and list of genotypes with this fitness value
    return (best_value, best_genotypes)

# Mutate given genotype
def mutate_genotype(genotype, matation_chance, cgp_setup):
    #print("Genotypes")
    #print(genotype)

    # Go through every block
    new_genotype = []
    for x in range(cgp_setup["inputs"], (cgp_setup["columns"] * cgp_setup["rows"]) + cgp_setup["inputs"]):
        block = []
        genotype_index = x - cgp_setup["inputs"]
        
        # Go through every block input
        for i in range(0, cgp_setup["block_inputs"]):
            # Test mutation chance
            if random.uniform(0, 1) < matation_chance:
                inputs_array = []

                for j in range(cgp_setup["inputs"]):
                    inputs_array.append(j)
            
                last_possible_block = (cgp_setup["rows"] * (((x - cgp_setup["inputs"]) // cgp_setup["rows"]))) + cgp_setup["inputs"]
                first_possible_block = max(cgp_setup["inputs"], last_possible_block - (cgp_setup["l_back"] * cgp_setup["rows"]))

                for j in range(first_possible_block, last_possible_block):
                    inputs_array.append(j)

                block.append(random.choice(inputs_array))
            else:
                block.append(genotype[genotype_index][i])
        
        # Test function mutation
        if random.uniform(0, 1) < matation_chance:
            block.append(random.choice(cgp_setup["cgp_functions"]))
        else:
            block.append(genotype[genotype_index][-1])

        new_genotype.append(block)

    # Output mutation
    outputs = []
    for x in range(cgp_setup["outputs"]):
        if random.uniform(0, 1) < matation_chance:
            inputs_array = []
            last_possible_block = (cgp_setup["columns"] * cgp_setup["rows"]) + cgp_setup["inputs"]
            first_possible_block = max(cgp_setup["inputs"], last_possible_block - (cgp_setup["l_back"] * cgp_setup["rows"]))

            for j in range(cgp_setup["inputs"]):
                inputs_array.append(j)

            for j in range(first_possible_block, last_possible_block):
                inputs_array.append(j)

            outputs.append(random.choice(inputs_array))
        else:
            outputs.append(genotype[-1][x])

    new_genotype.append(outputs)

    #print(genotype)
    #print(new_genotype)

    return new_genotype

# Create new population from best genotype
def make_new_population(parent_genotype, population_size, matation_chance, cgp_setup):
    new_population = []

    # Generate given number of genotypes
    for x in range(0, population_size):
        new_population.append(mutate_genotype(parent_genotype, matation_chance, cgp_setup))

    return new_population

# Run CGP with specified parameters
def run_cgp(genotype_file, generations_count, population_size, matation_chance, maximal_average_arithmetic_error, runs, cgp_setup):
    global optimaze_size
    global swith_fitness

    run_number = 0
    best_genotype = None
    best_fitness = inf

    for y in range(0, runs):
        run_number += 1
        best_run_genotype = None
        best_run_fitness = inf
        print("Executing run = " + str(run_number))

        # Inicialize fitness
        optimaze_size = False
        swith_fitness = False

        # Get start population
        start_population = get_start_population(population_size, genotype_file, cgp_setup)
        #print(start_population)

        # Get fitness values of population
        population_fitnesses = get_population_fitness(start_population, maximal_average_arithmetic_error, cgp_setup)
        #print(population_fitnesses)

        # Get best genotypes
        best_run_fitness, best_genotypes = get_best_genotypes(start_population, population_fitnesses)
        #print(best_run_fitness)
        #print(len(best_genotypes))

        # Select parent genotype
        best_run_genotype = random.choice(best_genotypes)

        # Run generations
        for x in  range(0, generations_count):
            # Switch fitness if average arithmetic error reached
            if (optimaze_size == False):
                if (swith_fitness):
                    print("Switching fitness")
                    best_run_fitness = used_blocks(best_run_genotype, cgp_setup)

                    optimaze_size = True
                    swith_fitness = False

            # Make new population from best genotype
            #print(best_run_genotype)
            new_population = make_new_population(best_run_genotype, population_size, matation_chance, cgp_setup)

            # Get fitness values of population
            population_fitnesses = get_population_fitness(new_population, maximal_average_arithmetic_error, cgp_setup)
            #print(population_fitnesses)

            # Get best genotypes
            new_best_fitness, best_genotypes = get_best_genotypes(new_population, population_fitnesses)

            # Select new paremt
            if (new_best_fitness < best_run_fitness):
                best_run_genotype = random.choice(best_genotypes)
                best_run_fitness = new_best_fitness
                print(best_run_fitness)
            elif (new_best_fitness == best_run_fitness):
                best_genotypes.append(best_run_genotype)
                best_run_genotype = random.choice(best_genotypes)

        # Update global best values if better genotype found
        if (optimaze_size):
            if (best_fitness > best_run_fitness):
                best_fitness = best_run_fitness
                best_genotype = best_run_genotype
                print(best_genotype)

            # Print best genotype of run
            print("Best fitness in run = "+str(best_run_fitness))
            print("Best overall fitness = "+str(best_fitness))
            print("##############################")
        else:
            print("Bad run")
            print("##############################")

    return (best_fitness, best_genotype)

####################### Main #############################
def main():
    random.seed(time.time())

    # Get arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("initialization", help="Initialization of CGP in .json file")
    parser.add_argument("--genotype", help="Genotype used in first population. When not given, all genotypes are randomly generated")
    parser.add_argument("--generations", help="Number of generations in one run, Default value is 50000", type=int, default=50000)
    parser.add_argument("--population", help="Size of population, Default value is 5", type=int, default=5)
    parser.add_argument("--mutation_chance", help="Chance of mutation for every gen. Default value is 0.1", type=int, default=0.1)
    parser.add_argument("--maximal_average_arithmetic_error", help="Maximal average arithmetic error of generated aproximation circuit. Default value is 0", type=float , default=0.0)
    parser.add_argument("--runs", help="Number of runs. Default value is 10", type=int , default=10)

    args = parser.parse_args()
    #print(args)

    # Read inicialization
    cgp_setup = get_inicialization(args.initialization)
    #print(cgp_setup)

    # Run CGP with given inicialization and return best found genotype
    best_fitness ,best_genotype = run_cgp(args.genotype, args.generations, args.population, args.mutation_chance, args.maximal_average_arithmetic_error, args.runs, cgp_setup)
    print("Best found overall:")
    print("Fitness = "+str(best_fitness))
    print(best_genotype)
    print("Used blocks = " + str(used_blocks(best_genotype, cgp_setup)))

if __name__ == "__main__":
    main()

# END CGP.py