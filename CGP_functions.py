# CGP_functions.py
# Approximation of arithmetic circuits in alternative representations, BIN, 29.4.2022
# Author: Matěj Kudera, xkuder04, VUT FIT
# Truth tables for CGP functions

cgp_truth_tables = [
    ########## 2 input function for normal representation ########
    {"00": 0, "01": 0, "10": 0, "11": 1}, # AND, index = 0
    {"00": 1, "01": 1, "10": 1, "11": 0}, # NAND, index = 1
    {"00": 0, "01": 1, "10": 1, "11": 1}, # OR, index = 2
    {"00": 1, "01": 0, "10": 0, "11": 0}, # NOR, index = 3
    {"00": 0, "01": 1, "10": 1, "11": 0}, # XOR, index = 4
    {"00": 1, "01": 0, "10": 0, "11": 1}, # XNOR, index = 5
    {"00": 1, "01": 1, "10": 0, "11": 0}, # NOT first input, index = 6
    {"00": 1, "01": 0, "10": 1, "11": 0}, # NOT second input, index = 7

    ########## 2 input functions for AND-INVERT representation ########
    # AND gates with invertions on diferent cables
    {"00": 0, "01": 0, "10": 0, "11": 1}, # AND, index = 8
    {"00": 0, "01": 1, "10": 0, "11": 0}, # AND first cable invertion, index = 9
    {"00": 0, "01": 0, "10": 1, "11": 0}, # AND second cable invertion, index = 10
    {"00": 1, "01": 1, "10": 1, "11": 0}, # AND output cable invertion, index = 11
    {"00": 1, "01": 0, "10": 0, "11": 0}, # AND first and second cable invertion, index = 12
    {"00": 1, "01": 0, "10": 1, "11": 1}, # AND first and output cable invertion, index = 13
    {"00": 1, "01": 1, "10": 0, "11": 1}, # AND second and output cable invertion, index = 14
    {"00": 0, "01": 1, "10": 1, "11": 1}, # AND all cables invertion, index = 15

    ########## 3 input functions for MAJORITY-INVERT representation ########
    # MAJORITY with invertions on diferent cables
    {"000": 0, "001": 0, "010": 0, "011": 1, "100": 0, "101": 1, "110": 1, "111": 1}, # MAJORITY, index = 16
    {"000": 0, "001": 1, "010": 1, "011": 1, "100": 0, "101": 0, "110": 0, "111": 1}, # MAJORITY first cable invertion, index = 17
    {"000": 0, "001": 1, "010": 0, "011": 0, "100": 1, "101": 1, "110": 0, "111": 1}, # MAJORITY second cable invertion, index = 18
    {"000": 0, "001": 0, "010": 1, "011": 0, "100": 1, "101": 0, "110": 1, "111": 1}, # MAJORITY third cable invertion, index = 19
    {"000": 1, "001": 1, "010": 0, "011": 1, "100": 0, "101": 1, "110": 0, "111": 0}, # MAJORITY first and second cable invertion, index = 20
    {"000": 1, "001": 0, "010": 1, "011": 1, "100": 0, "101": 0, "110": 1, "111": 0}, # MAJORITY first and third cable invertion, index = 21
    {"000": 1, "001": 0, "010": 0, "011": 0, "100": 1, "101": 1, "110": 1, "111": 0}, # MAJORITY second and third cable invertion, index = 22
    {"000": 1, "001": 1, "010": 1, "011": 0, "100": 1, "101": 0, "110": 0, "111": 0} # MAJORITY all input cables invertion, index = 23
]

# END CGP_functions.py