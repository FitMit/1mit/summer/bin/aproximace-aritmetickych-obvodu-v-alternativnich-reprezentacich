BIN - Aproximace aritmetických obvodů v alternativních reprezentacích
---------

Program umožňující hledání obvodů podle zadaných pravdivostních tabulek se zadanou maximální aritmetickou chybou. Po nalezení obvodu se program následně snaží ještě obvod co nejvíce zmenšit. Program využívá CGP a obvody je možné skládat ze standartních, AND-INVERT nebo MAJORITY-INVERT prvků.

Body: 16/25
- Neefektivní implementace
- Slabé provedení experimentální části
