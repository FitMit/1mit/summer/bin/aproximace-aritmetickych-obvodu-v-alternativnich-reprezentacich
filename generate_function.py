# generate_function.py
# Approximation of arithmetic circuits in alternative representations, BIN, 29.4.2022
# Author: Matěj Kudera, xkuder04, VUT FIT
# Generate truth table for function

inputs = 8
outputs = 8
result_dict = {}

# Generate all combinations
for x in range(0, 2**(inputs)):
    binary = str(bin(x))[2:].zfill(inputs)

    # Get A and B
    #c = int(binary[:1], 2)
    a = int(binary[:4], 2)
    b = int(binary[-4:], 2)

    # Use function
    if b == 0:
        q = 0
    else:
        q = a // b

    r = a - b*q

    # Make binary result
    b_q_result = str(bin(q))[2:].zfill(4)
    b_r_result = str(bin(r))[2:].zfill(4)

    # Store item
    result_dict[binary] = b_q_result + b_r_result

print(result_dict)

# END generate_function.py